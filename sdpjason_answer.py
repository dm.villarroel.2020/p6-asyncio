import asyncio
import json

async def send_answer():
    answer = {
        "type": "answer",
        "sdp": {
            'version': 0,
            'origin': {'username': 'user', 'sessionId': 434344, 'sessionVersion': 0, 'netType': 'IN', 'ipVer': 4, 'address': '127.0.0.1'},
            'name': 'Session',
            'timing': {'start': 0, 'stop': 0},
            'connection': {'version': 4, 'ip': '127.0.0.1'},
            'media': [
                {
                    'rtp': [{'payload': 0, 'codec': 'PCMU', 'rate': 8000}, {'payload': 96, 'codec': 'opus', 'rate': 48000}],
                    'type': 'audio',
                    'port': 34543,  # Change port to 34543
                    'protocol': 'RTP/SAVPF',
                    'payloads': '0 96',
                    'ptime': 20,
                    'direction': 'sendrecv'
                },
                {
                    'rtp': [{'codec': 'H264', 'payload': 97, 'rate': 90000}, {'codec': 'VP8', 'payload': 98, 'rate': 90000}],
                    'type': 'video',
                    'port': 34543,  # Change port to 34543
                    'protocol': 'RTP/SAVPF',
                    'payloads': '97 98',
                    'direction': 'sendrecv'
                }
            ]
        }
    }

    return json.dumps(answer)

async def main():
    sdp_answer = await send_answer()
    print("Sending SDP Answer:")
    print(sdp_answer)

if __name__ == "__main__":
    asyncio.run(main())
